/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package watermarkpdf;

import com.itextpdf.awt.geom.AffineTransform;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfRectangle;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;





/**
 *
 * @author user
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField tfPhrase;
    @FXML
    private TextField tf_append;
    @FXML
    private TextField tf_PagePrint;
    @FXML
    private TextField tf_PathFile;
    @FXML
    private Label lbl_Result;
    @FXML
    private CheckBox cbox_LU;
    @FXML
    private Button btn_Go;
    
    

    @FXML
    private void handleButtonAction(ActionEvent event) {
        try {
            manipulatePdf(tf_PathFile.getText(), 
                    tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_resize.pdf");
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            addStamp();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        lbl_Result.setText("Обработка документа завершена");
    }

    @FXML
    private void ButtonOpenFile(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть файл");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("PDF файлы", "*.pdf"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            tf_PathFile.setText(selectedFile.getPath());
            btn_Go.setDisable(false);
        }
    }

  public void manipulatePdf(String src, String dest)
        throws IOException, DocumentException {
    	// reader for the src file
        PdfReader reader = new PdfReader(src);
        // initializations
        Rectangle pageSize = reader.getPageSize(1);
        Document document = new Document(pageSize, 0, 0, 0, 0);        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(String.format(dest)));
        document.open();
        PdfContentByte cb = writer.getDirectContent();
        PdfImportedPage page;
        Rectangle currentSize;        
        int total = reader.getNumberOfPages();
        for (int i = 0; i < total; ) {
            document.newPage();
            currentSize = reader.getPageSize(++i);
            page = writer.getImportedPage(reader, i);            
            cb.addTemplate(page, 0.85, 0, 0, 0.85, 35.0, 35.0);
            //документ, масштаб по осиХ. вращение, угол наклона, масштаб по оси У, отступ слева и справа
        }        
        document.close();
        reader.close();
    }
   
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        final Tooltip tooltip = new Tooltip();
tooltip.setText(
    "ЛУ=1, ТУ=2 или ТУ=1 если нет ЛУ \n" +
    "для остальных страниц +1\n");
tf_PagePrint.setTooltip(tooltip);
    }
  
private void addStamp() throws IOException, DocumentException{
    if (tfPhrase.getLength() > 0) {
            lbl_Result.setText("");
            PdfReader reader = new PdfReader(tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_resize.pdf");        
            Rectangle mediabox = reader.getPageSize(1);        
            PdfStamper stamper = new PdfStamper(reader, 
                    new FileOutputStream(tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_out.pdf"));
            stamper.setRotateContents(false);
            BaseFont bf = BaseFont.createFont("TNR.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf);
            //int iPos = tfPhrase.getText().indexOf('#');            
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                PdfContentByte canvas = stamper.getOverContent(i);
                if (tfPhrase.getLength() > 0) {                    
                    if (cbox_LU.isSelected()) {
                        switch (i) {
                            case 1:
                                ColumnText.showTextAligned(canvas,
                                        Element.ALIGN_LEFT, new Phrase(("Замененные листы (Лист утверждения "
                                                + tfPhrase.getText() + ")"), font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                                break;
                            case 2:
                                ColumnText.showTextAligned(canvas,
                                        Element.ALIGN_LEFT, new Phrase(("Замененные листы (Титульный лист "
                                                + tfPhrase.getText() + ")"), font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                                break;
                            default:
                                ColumnText.showTextAligned(canvas,
                                        Element.ALIGN_LEFT, new Phrase(("Замененные листы (Лист "
                                                + (i - 1) + " " + tfPhrase.getText() + ")"),
                                                font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                        }
                    } else {
                        if (i==1)
                                ColumnText.showTextAligned(canvas,
                                        Element.ALIGN_LEFT, new Phrase(("Замененные листы (Титульный лист "
                                                + tfPhrase.getText() + ")"), font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                        else
                                ColumnText.showTextAligned(canvas,
                                        Element.ALIGN_LEFT, new Phrase(("Замененные листы (Лист "
                                                + (i - 1) + " " + tfPhrase.getText() + ")"), font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                    }

                } else {
                   ColumnText.showTextAligned(canvas,
                        Element.ALIGN_LEFT, new Phrase(tfPhrase.getText(), font), mediabox.getRight()/3, mediabox.getTop()-60, 0);
                }                
                 ColumnText.showTextAligned(canvas,
                            Element.ALIGN_LEFT, new Phrase(tf_append.getText(), font), mediabox.getRight()*2/3, mediabox.getTop()-40, 0);
            }                
            stamper.close();
            reader.close();
            File f1 = new File(tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_resize.pdf");
            f1.delete();
             if (tf_PagePrint.getLength()>0){
             reader = new PdfReader(tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_out.pdf");
             reader.selectPages(tf_PagePrint.getText());
             stamper = new PdfStamper(reader, new FileOutputStream(tf_PathFile.getText().substring(0,tf_PathFile.getLength()-4)+"_out_cut.pdf"));             
             stamper.close();
             reader.close();
            }            
        }
}
    
    
}


